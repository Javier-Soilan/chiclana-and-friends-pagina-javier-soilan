/* --------------------------display none------------------------------------------- */
const view = { viewNow: 'home' };

function listener() {
    document.querySelector('#btn__home').addEventListener('click', viewHome);
    document.querySelector('#btn__ep').addEventListener('click', viewEp);
    document.querySelector('#btn__about').addEventListener('click', viewAbout);
    /* document.querySelector('#btn__news').addEventListener('click', viewNews); */
    document.querySelector('#sopas').addEventListener('click', viewSopas);
    document.querySelector('#pep').addEventListener('click', viewPep);
    document.querySelector('#pui').addEventListener('click', viewPui);
    document.querySelector('#submit').addEventListener('submit', filterEpisodes);
    
}

viewHome = () => {
    console.log('viewHome');
    if (view.viewNow === 'home') {
        alert('Welcome home');
    } else {
        view.viewNow = 'home';
        document.querySelector('#home').classList.remove('display__none');
        document.querySelector('#episodes').classList.add('display__none');
        document.querySelector('#about').classList.add('display__none');
        document.querySelector('#news').classList.add('display__none');
        document.querySelector('#about__sopas').classList.add('display__none');
        document.querySelector('#about__pep').classList.add('display__none');
        document.querySelector('#about__pui').classList.add('display__none');
        document.querySelector('#detail').classList.add('display__none');
        putodiv$$.innerHTML = '';
    }

};

viewEp = () => {
    console.log('viewEp');
    if (view.viewNow === 'episodes') {
        alert('All Episodes');
    } else {
        view.viewNow = 'episodes';
        document.querySelector('#home').classList.add('display__none');
        document.querySelector('#episodes').classList.remove('display__none');
        document.querySelector('#about').classList.add('display__none');
        document.querySelector('#news').classList.add('display__none');
        document.querySelector('#about__sopas').classList.add('display__none');
        document.querySelector('#about__pep').classList.add('display__none');
        document.querySelector('#about__pui').classList.add('display__none');
        document.querySelector('#detail').classList.add('display__none');
        putodiv$$.innerHTML = '';
    }

};

viewAbout = () => {
    console.log('viewAbout');
    if (view.viewNow === 'about') {
        alert('Know more about us');
    } else {
        view.viewNow = 'about';
        document.querySelector('#home').classList.add('display__none');
        document.querySelector('#episodes').classList.add('display__none');
        document.querySelector('#about').classList.remove('display__none');
        document.querySelector('#news').classList.add('display__none');
        document.querySelector('#about__sopas').classList.add('display__none');
        document.querySelector('#about__pep').classList.add('display__none');
        document.querySelector('#about__pui').classList.add('display__none');
        document.querySelector('#detail').classList.add('display__none');
        putodiv$$.innerHTML = '';
    }

};

viewNews = () => {
    console.log('viewNews');
    if (view.viewNow === 'news') {
        alert('Videogames news');
    } else {
        view.viewNow = 'news';
        document.querySelector('#home').classList.add('display__none');
        document.querySelector('#episodes').classList.add('display__none');
        document.querySelector('#about').classList.add('display__none');
        document.querySelector('#news').classList.remove('display__none');
        document.querySelector('#about__sopas').classList.add('display__none');
        document.querySelector('#about__pep').classList.add('display__none');
        document.querySelector('#about__pui').classList.add('display__none');
        document.querySelector('#detail').classList.add('display__none');
        putodiv$$.innerHTML = '';
    }

};

viewDetails = () => {
    console.log('viewDetail');
    /* if (view.viewNow === 'detail') {
        alert('Video detail');
    } else { */
        view.viewNow = 'detail';
        document.querySelector('#home').classList.add('display__none');
        document.querySelector('#episodes').classList.add('display__none');
        document.querySelector('#about').classList.add('display__none');
        document.querySelector('#news').classList.add('display__none');
        document.querySelector('#about__sopas').classList.add('display__none');
        document.querySelector('#about__pep').classList.add('display__none');
        document.querySelector('#about__pui').classList.add('display__none');
        document.querySelector('#detail').classList.remove('display__none');
        putodiv$$.innerHTML ='';
   /*  } */
};

viewSopas = () => {
    console.log('viewSopas');
    if (view.viewNow === 'sopas') {
        alert('About Sopas');
    } else {
        view.viewNow = 'sopas';
        document.querySelector('#home').classList.add('display__none');
        document.querySelector('#episodes').classList.add('display__none');
        document.querySelector('#about').classList.add('display__none');
        document.querySelector('#news').classList.add('display__none');
        document.querySelector('#about__sopas').classList.remove('display__none');
        document.querySelector('#about__pep').classList.add('display__none');
        document.querySelector('#about__pui').classList.add('display__none');
        putodiv$$.innerHTML = '';
    }
};

viewPep = () => {
    console.log('viewPep');
    if (view.viewNow === 'pep') {
        alert('About Pep');
    } else {
        view.viewNow = 'pep';
        document.querySelector('#home').classList.add('display__none');
        document.querySelector('#episodes').classList.add('display__none');
        document.querySelector('#about').classList.add('display__none');
        document.querySelector('#news').classList.add('display__none');
        document.querySelector('#about__sopas').classList.add('display__none');
        document.querySelector('#about__pep').classList.remove('display__none');
        document.querySelector('#about__pui').classList.add('display__none');
        putodiv$$.innerHTML = '';
    }
};

viewPui = () => {
    console.log('viewpui');
    if (view.viewNow === 'pui') {
        alert('About Pui');
    } else {
        view.viewNow = 'pui';
        document.querySelector('#home').classList.add('display__none');
        document.querySelector('#episodes').classList.add('display__none');
        document.querySelector('#about').classList.add('display__none');
        document.querySelector('#news').classList.add('display__none');
        document.querySelector('#about__sopas').classList.add('display__none');
        document.querySelector('#about__pep').classList.add('display__none');
        document.querySelector('#about__pui').classList.remove('display__none');
        putodiv$$.innerHTML = '';
    }

};
/* --------------------------------------------bucle episodios--------------------------- */

function addEpisode() {
    const epViewList$$ = document.querySelector('.ep__view__list');

    episodes.forEach(episode => {
        const newA$$ = document.createElement('a')
        newA$$.classList.add('ep__view__list__img');
        newA$$.setAttribute('href', episode.linkYT);
        newA$$.setAttribute('target', '_blank');
        newA$$.innerHTML = `
            <img src="${episode.imgUrl}" alt="episode">
            <p>${episode.text}</p>
            `
        epViewList$$.appendChild(newA$$);
    })
}
window.onload = function () {
    addEpisode();
    listener();
}


/* ------------------------------------busqueda episodios---------------------------- */



let filterEpisodes = ($event) => {


    $event.preventDefault()
    console.log('click')
    var input$$ = document.querySelector('#search').value;
    var lowerCase = input$$.toLowerCase();
    var putodiv$$ = document.querySelector('#puto__div')
    episodes.forEach(episode => {

        if (episode.text.toLowerCase().includes(lowerCase)) {
            putodiv$$.innerHTML = `  <iframe width="1020.096" height="573.804"  frameborder="0" src="${episode.video}" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen id="youtube"></iframe>`;

            viewDetails();
        }


    });

}


/* ----------------------------lista episodios-------------------------------------- */

const episodes = [

    { imgUrl: './assets/episodes/ep-104.jpg', text: 'Episodio 104', linkYT: 'https://www.youtube.com/watch?v=mFsBJrSDjX0&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/mFsBJrSDjX0' },

    { imgUrl: './assets/episodes/ep-103.jpg', text: 'Episodio 103', linkYT: 'https://www.youtube.com/watch?v=DYkzxR0cZGQ&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/DYkzxR0cZGQ' },

    { imgUrl: './assets/episodes/ep-102.jpg', text: 'Episodio 102', linkYT: 'https://www.youtube.com/watch?v=H9Y1bti6WkQ&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/H9Y1bti6WkQ' },

    { imgUrl: './assets/episodes/ep-101.jpg', text: 'Episodio 101', linkYT: 'https://www.youtube.com/watch?v=OedRGe4F6io&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/OedRGe4F6io'},

    { imgUrl: './assets/episodes/ep-100.jpg', text: 'Episodio 100', linkYT: 'https://www.youtube.com/watch?v=qQfHz42BVHg&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/qQfHz42BVHg'},

    { imgUrl: './assets/episodes/ep-99.jpg', text: 'Episodio 99', linkYT: 'https://www.youtube.com/watch?v=6OF7GKsMIug&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/6OF7GKsMIug'},

    { imgUrl: './assets/episodes/ep-98.jpg', text: 'Episodio 98', linkYT: 'https://www.youtube.com/watch?v=b892u9V4lLY&t=42s&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/b892u9V4lLY'},

    { imgUrl: './assets/episodes/ep-97.jpg', text: 'Episodio 97', linkYT: 'https://www.youtube.com/watch?v=aBgZFYgnr0w&t=25s&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/aBgZFYgnr0w'},

    { imgUrl: './assets/episodes/ep-96.jpg', text: 'Episodio 96', linkYT: 'https://www.youtube.com/watch?v=Bxenw6PAr3M&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/Bxenw6PAr3M'},

    { imgUrl: './assets/episodes/ep-95.jpg', text: 'Episodio 95', linkYT: 'https://www.youtube.com/watch?v=0hDIKJ1qw_k&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/0hDIKJ1qw_k'},

    { imgUrl: './assets/episodes/ep-94.jpg', text: 'Episodio 94', linkYT: 'https://www.youtube.com/watch?v=Y8dQRYDgBLo&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/Y8dQRYDgBLo' },

    { imgUrl: './assets/episodes/ep-enero.jpg', text: 'Mejores momentos Enero 2021', linkYT: 'https://www.youtube.com/watch?v=Idiq0iwRI64&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/Idiq0iwRI64'},

    { imgUrl: './assets/episodes/ep-93.jpg', text: 'Episodio 93', linkYT: 'https://www.youtube.com/watch?v=ohuRFM4pkw8&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/ohuRFM4pkw8' },

    { imgUrl: './assets/episodes/ep-92.jpg', text: 'Episodio 92', linkYT: 'https://www.youtube.com/watch?v=dJlgQXuaAls&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/dJlgQXuaAls' },

    { imgUrl: './assets/episodes/ep-91.jpg', text: 'Episodio 91', linkYT: 'https://www.youtube.com/watch?v=ojpYL2B1A-k&t=25s&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/ojpYL2B1A-k'},

    { imgUrl: './assets/episodes/ep-90.jpg', text: 'Episodio 90', linkYT: 'https://www.youtube.com/watch?v=UZCJ7dvPUEU&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/UZCJ7dvPUEU'},

    { imgUrl: './assets/episodes/ep-89.jpg', text: 'Episodio 89', linkYT: 'https://www.youtube.com/watch?v=Ee15LwyOtVM&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/Ee15LwyOtVM'},

    { imgUrl: './assets/episodes/ep-88.jpg', text: 'Episodio 88', linkYT: 'https://www.youtube.com/watch?v=ZUvuF8VeKxI&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/ZUvuF8VeKxI'},

    { imgUrl: './assets/episodes/ep-87.jpg', text: 'Episodio 87', linkYT: 'https://www.youtube.com/watch?v=FiQo4duTxjU&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/FiQo4duTxjU' },

    { imgUrl: './assets/episodes/ep-86.jpg', text: 'Episodio 86', linkYT: 'https://www.youtube.com/watch?v=wyDJHBRGAQA&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/wyDJHBRGAQA'},

    { imgUrl: './assets/episodes/ep-85.jpg', text: 'Episodio 85', linkYT: 'https://www.youtube.com/watch?v=5So2wHuZ0H8&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/5So2wHuZ0H8'},

    { imgUrl: './assets/episodes/ep-84.jpg', text: 'Episodio 84', linkYT: 'https://www.youtube.com/watch?v=diUpy_NJCko&t=54s&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/diUpy_NJCko' },

    { imgUrl: './assets/episodes/ep-83.jpg', text: 'Episodio 83', linkYT: 'https://www.youtube.com/watch?v=Ud6Pwu2SC98&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/diUpy_NJCko'},

    { imgUrl: './assets/episodes/ep-82.jpg', text: 'Episodio 82', linkYT: 'https://www.youtube.com/watch?v=Y2jnOa0JwEY&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/Ud6Pwu2SC98'},

    { imgUrl: './assets/episodes/ep-diciembre.jpg', text: 'Mejores momentos Diciembre 2020', linkYT: 'https://www.youtube.com/watch?v=KNlIuypCMEw&t=9s&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/KNlIuypCMEw' },

    { imgUrl: './assets/episodes/ep-81.jpg', text: 'Episodio 81', linkYT: 'https://www.youtube.com/watch?v=LA36HXNfiqY&t=10s&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/LA36HXNfiqY' },

    { imgUrl: './assets/episodes/ep-80.jpg', text: 'Episodio 80', linkYT: 'https://www.youtube.com/watch?v=hnyqIPGt85I&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/hnyqIPGt85I'},

    { imgUrl: './assets/episodes/ep-79.jpg', text: 'Episodio 79', linkYT: 'https://www.youtube.com/watch?v=sE19g-8Ibh0&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/sE19g-8Ibh0'},

    { imgUrl: './assets/episodes/ep-78.jpg', text: 'Episodio 78', linkYT: 'https://www.youtube.com/watch?v=-4MtOBlhwks&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/-4MtOBlhwks'},

    { imgUrl: './assets/episodes/ep-77.jpg', text: 'Episodio 77', linkYT: 'https://www.youtube.com/watch?v=zBTQrDXP6jY&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/zBTQrDXP6jY' },

    { imgUrl: './assets/episodes/ep-chiri.jpg', text: 'Gala ChiriGOTY 2020 REMASTERED', linkYT: 'https://www.youtube.com/watch?v=7m2_vfAtR1Q&t=2510s&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/7m2_vfAtR1Q' },

    { imgUrl: './assets/episodes/ep-76.jpg', text: 'Episodio 76', linkYT: 'https://www.youtube.com/watch?v=Yu1WUmf2m94&t=481s&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/Yu1WUmf2m94' },

    { imgUrl: './assets/episodes/ep-75.jpg', text: 'Episodio 75', linkYT: 'https://www.youtube.com/watch?v=JBiJhNukEqA&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/JBiJhNukEqA'},

    { imgUrl: './assets/episodes/ep-74.jpg', text: 'Episodio 74', linkYT: 'https://www.youtube.com/watch?v=UPzJ1_jniKI&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/UPzJ1_jniKI'},

    { imgUrl: './assets/episodes/ep-73.jpg', text: 'Episodio 73', linkYT: 'https://www.youtube.com/watch?v=wjNe0FhoKCk&t=56s&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/wjNe0FhoKCk' },

    { imgUrl: './assets/episodes/ep-72.jpg', text: 'Episodio 72', linkYT: 'https://www.youtube.com/watch?v=YzTgZiGdAdk&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/YzTgZiGdAdk'},

    { imgUrl: './assets/episodes/ep-71.jpg', text: 'Episodio 71', linkYT: 'https://www.youtube.com/watch?v=AmFbLPjv-Js&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/AmFbLPjv-Js'},

    { imgUrl: './assets/episodes/ep-game.jpg', text: 'Episodio Game Awards', linkYT: 'https://www.youtube.com/watch?v=DkmuUGOWegM&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/DkmuUGOWegM'},

    { imgUrl: './assets/episodes/ep-70.jpg', text: 'Episodio 70', linkYT: 'https://www.youtube.com/watch?v=Lxogl__Yacg&ab_channel=Chiclana%26Friends' , 
    video: 'https://www.youtube.com/embed/Lxogl__Yacg'},

    { imgUrl: './assets/episodes/ep-69.jpg', text: 'Episodio 69', linkYT: 'https://www.youtube.com/watch?v=-vCGXA3PcCs&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/-vCGXA3PcCs' },

    { imgUrl: './assets/episodes/ep-68.jpg', text: 'Episodio 68', linkYT: 'https://www.youtube.com/watch?v=df5OESNi8Vk&ab_channel=Chiclana%26Friends', 
    video: 'https://www.youtube.com/embed/df5OESNi8Vk' },

]